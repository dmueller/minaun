"""
    A collection of functions for plotting
"""
import numpy as np
from numpy import pi
import pylab as plt
from colorsys import hls_to_rgb
import minaun.cy.observables as observables
import minaun.cy.superconductor as superconductor


# from https://stackoverflow.com/questions/17044052/mathplotlib-imshow-complex-2d-array
def colorize(z):
    r = np.abs(z)
    arg = np.angle(z)

    h = (arg + pi)  / (2 * pi) + 0.5
    l = 1.0 - 1.0/(1.0 + r**0.3)
    s = 0.8

    c = np.vectorize(hls_to_rgb) (h,l,s) # --> tuple
    c = np.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
    c = c.swapaxes(0,2)
    return c


def phi_phase_plot(s):
    nx, ny = s.dims
    p_re = s.p[::2]
    p_im = s.p[1::2]
    p_complex = p_re + 1j * p_im

    return colorize(p_complex.reshape(nx, ny)[1:-1, 1:-1].T)
