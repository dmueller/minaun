#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    U(1) functions
"""

#cimport cython
#import cython
#import numpy as np
#cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt, acos, asin, atan

cdef inline void mul2(double* a, double* b, long ta, long tb, double* r) nogil:
    r[0] = a[0] * b[0] - ta * tb * a[1] * b[1]
    r[1] = tb * a[0] * b[1] + ta * a[1] * b[0]

cdef inline void mul4(double* a, double *b, double *c, double *d, long ta, long tb, long tc, long td, double *r) nogil:
    cdef double ab[2]
    cdef double cd[2]
    mul2(a, b, ta, tb, ab)
    mul2(c, d, tc, td, cd)
    mul2(ab, cd, 1, 1, r)

cdef inline void cexp(double a, double f, double *r) nogil:
    r[0] = f*cos(a)
    r[1] = f*sin(a)

cdef inline double clog(double *a) nogil:
    return atan(a[1] / a[0])

cdef inline double cabs(double *a) nogil:
    return a[0] * a[0] + a[1] * a[1]

cdef inline double cre(double *a) nogil:
    return a[0]

cdef inline double cim(double *a) nogil:
    return a[1]

cdef inline void cunit(double *a) nogil:
    a[0] = 1.0
    a[1] = 0.0

cdef inline void czero(double *a) nogil:
    a[0] = 0.0
    a[1] = 0.0

cdef inline void cset(double *a, double f, double *r) nogil:
    r[0] = f * a[0]
    r[1] = f * a[1]

cdef inline void cadd(double *a, double f, double *r) nogil:
    r[0] += f * a[0]
    r[1] += f * a[1]
