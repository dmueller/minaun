"""
    Header file for lattice gauge theory functions
"""

cdef void d1f(long x, long i, double *phi, long* dims, double *r) nogil
cdef void d1b(long x, long i, double *phi, long* dims, double *r) nogil
cdef void d2(long x, long i, double *phi, long* dims, double *r) nogil
cdef void D1f(long x, long i, double *phi, double *u, long* dims, double *r) nogil
cdef void D1b(long x, long i, double *phi, double *u, long* dims, double *r) nogil
cdef void D2(long x, long i, double *phi, double *u, long* dims, double *r) nogil
cdef void plaq(long x, long i, long j, long oi, long oj, double* u, long* dims, double* r) nogil
cdef void plaq2(long x, long i, long j, long oi, long oj, double* u, long* dims, double* r) nogil
cdef void plaq_fast(long x, double* u, long* dims, double* r) nogil
cdef void c(long x, long i, long j, long oi, long oj, double* u, long* dims, double* r) nogil
