"""
    Header for U(1) functions
"""

cdef void mul2(double* a, double* b, long ta, long tb, double* r) nogil
cdef void mul4(double* a, double *b, double *c, double *d, long ta, long tb, long tc, long td, double *r) nogil
cdef void cexp(double a, double f, double *r) nogil
cdef double clog(double *a) nogil
cdef double cabs(double *a) nogil
cdef double cre(double *a) nogil
cdef double cim(double *a) nogil
cdef void cunit(double *a) nogil
cdef void czero(double *a) nogil
cdef void cset(double *a, double f, double *r) nogil
cdef void cadd(double *a, double f, double *r) nogil