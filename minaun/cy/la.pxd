"""
    Header for lattice functions
"""

cdef long index(long ix, long iy, long* dims) nogil
cdef long index_nm(long ix, long iy, long* dims) nogil
cdef void point(long x, long* dims, long* r) nogil
cdef long shift(long x, long i, long o, long* dims) nogil
cdef long mod(long i, long n) nogil
cdef long findex(long x) nogil
cdef long gindex(long x, long i) nogil
cdef long lindex(long x, long i, long oi, long* dims) nogil
cdef void lindex2(long x, long i, long oi, long* dims, long* xr, long *gr) nogil
