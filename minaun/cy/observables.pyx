#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    A module for "observables" in the largest sense
"""

cimport la
cimport lgt
cimport u1
import numpy as np
cimport numpy as cnp
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free
from libc.math cimport sin, cos, exp, sqrt, acos, asin, atan, atan2

cdef double fourpi = 4.0 * np.pi

def free_energy(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p = s.p
        cnp.ndarray[double, ndim=1, mode="c"] u = s.u
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        double f = 0.0
        double q = s.q
        double l = s.l
        double m = s.m
        double mu = s.mu
        double h = s.h
        long n = s.N
        long nx = dims[0]
        long ny = dims[1]
        long x, i, xf, ix, iy
        double *b0

    for ix in prange(1, nx-1, schedule='guided', nogil=True):
        b0 = <double *> malloc(2 * sizeof(double))
        for iy in range(1, ny-1):
            x = la.index(ix, iy, &dims[0])
            xf = la.findex(x)

            # scalar field contribution
            for i in range(2):
                lgt.D1f(x, i, &p[0], &u[0], &dims[0], b0)
                f += u1.cabs(b0)
            f += (m ** 2 - mu ** 2) * u1.cabs(&p[xf])
            f += l * u1.cabs(&p[xf]) ** 2

            # gauge field contribution
            lgt.c(x, 0, 1, 1, 1, &u[0], &dims[0], b0)
            f += u1.cabs(b0) / (2.0 * fourpi * q ** 2)

            # external magnetic field contribution
            f += - h / (fourpi * q**2) * b0[1]
        free(b0)

    return f / (nx-2) / (ny-2)

def phi_abs(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p = s.p
        cnp.ndarray[double, ndim=1, mode="c"] r = np.zeros(s.N, dtype=np.double)
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long x, ix, iy
        long n = s.N
        long nx = dims[0]
        long ny = dims[1]

    for ix in prange(1, nx-1, schedule='guided', nogil=True):
        for iy in range(1, ny-1):
            x = la.index(ix, iy, &dims[0])
            r[x] = u1.cabs(&p[la.findex(x)])

    return r.reshape((nx, ny))[1:-1, 1:-1]

def magnetic_field(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] u = s.u
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long x, ix, iy
        double q = s.q
        long n = s.N
        long nx = dims[0]
        long ny = dims[1]
        double *b0
        cnp.ndarray[double, ndim=1, mode="c"] r = np.zeros(s.N, dtype=np.double)


    for ix in prange(1, nx-1, schedule='guided', nogil=True):
        b0 = <double *> malloc(2 * sizeof(double))
        for iy in range(1, ny-1):
            x = la.index(ix, iy, &dims[0])
            lgt.plaq(x, 0, 1, 1, 1, &u[0], &dims[0], b0)
            r[x] = b0[1] / q
        free(b0)

    return r.reshape((nx, ny))[1:-1, 1:-1]