#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    Basic lattice functions (index operations, plaquettes)
"""

cimport u1

cdef inline long index(long ix, long iy, long* dims) nogil:
    return dims[1] * mod(ix, dims[0]) + mod(iy, dims[1])

cdef inline long index_nm(long ix, long iy, long* dims) nogil:
    return dims[1] * ix + iy

cdef inline void point(long x, long* dims, long* r) nogil:
    cdef long i
    r[1] = x % dims[1]
    r[0] = x / dims[1]

cdef inline long shift(long x, long i, long o, long* dims) nogil:
    #cdef long pos[2]
    #point(x, dims, pos)
    #pos[i] = (pos[i] + o + dims[i]) % dims[i]
    #return index_nm(pos[0], pos[1], dims)
    return x + o * (i + dims[1] * (1-i))

cdef inline long mod(long i, long n) nogil:
    return (i + n) % n

cdef inline long findex(long x) nogil:
    return 2*x

cdef inline long gindex(long x, long i) nogil:
    return 2*(2*x + i)

cdef inline long lindex(long x, long i, long oi, long* dims) nogil:
    if oi > 0:
        return gindex(x, i)
    else:
        return gindex(shift(x, i, -1, dims), i)

cdef void lindex2(long x, long i, long oi, long* dims, long* xr, long *gr) nogil:
    xr[0] = shift(x, i, oi, dims)
    if oi > 0:
        gr[0] = gindex(x, i)
    else:
        gr[0] = gindex(xr[0], i)
