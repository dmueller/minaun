#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    Functions for lattice gauge theory coupled to complex scalar field
"""

cimport u1
cimport la

"""
    First and second order finite differences
"""

# forward
cdef void d1f(long x, long i, double *phi, long* dims, double *r) nogil:
    cdef long xp
    cdef int c
    xp = la.shift(x, i, +1, dims)
    for c in range(2):
        r[c] = (phi[la.findex(xp) + c] - phi[la.findex(x) + c])

# backward
cdef void d1b(long x, long i, double *phi, long* dims, double *r) nogil:
    cdef long xm
    cdef int c
    xm = la.shift(x, i, -1, dims)
    for c in range(2):
        r[c] = (phi[la.findex(x) + c] - phi[la.findex(xm) + c])


# central
cdef void d2(long x, long i, double *phi, long* dims, double *r) nogil:
    cdef long xp
    cdef long xm
    cdef int c
    xp = la.shift(x, i, +1, dims)
    xm = la.shift(x, i, -1, dims)
    for c in range(2):
        r[c] = phi[la.findex(xp)+c] + phi[la.findex(xm)+c] - 2 * phi[la.findex(x)+c]

"""
    First and second order gauge-covariant finite differences
"""

# forward
cdef void D1f(long x, long i, double *phi, double *u, long *dims, double *r) nogil:
    cdef long xp
    xp = la.shift(x, i, +1, dims)
    u1.mul2(&u[la.lindex(x, i, +1, dims)], &phi[la.findex(xp)], 1, 1, r)
    u1.cadd(&phi[la.findex(x)], -1, r)

# backward
cdef void D1b(long x, long i, double *phi, double *u, long *dims, double *r) nogil:
    cdef long xm
    xm = la.shift(x, i, -1, dims)
    u1.mul2(&u[la.lindex(x, i, -1, dims)], &phi[la.findex(xm)], -1, 1, r)
    r[0] *= -1
    r[1] *= -1
    u1.cadd(&phi[la.findex(x)], 1, r)

# central
cdef void D2(long x, long i, double *phi, double *u, long* dims, double *r) nogil:
    cdef long xp, xm
    cdef double b0[2]
    cdef double b1[2]

    xp = la.shift(x, i, +1, dims)
    xm = la.shift(x, i, -1, dims)
    u1.czero(r)
    u1.mul2(&u[la.lindex(x, i, +1, dims)], &phi[la.findex(xp)], 1, 1, &b0[0])
    u1.cadd(&b0[0], 1, r)
    u1.mul2(&u[la.lindex(x, i, -1, dims)], &phi[la.findex(xm)], -1, 1, &b0[0])
    u1.cadd(&b0[0], 1, r)
    u1.cadd(&phi[la.findex(x)], -2, r)

"""
    Plaquettes
"""

cdef void plaq(long x, long i, long j, long oi, long oj, double* u, long* dims, double* r) nogil:
    cdef long x0, x1, x2, x3, g0, g1, g2, g3
    x0 = x
    la.lindex2(x0, i, +oi, dims, &x1, &g0)
    la.lindex2(x1, j, +oj, dims, &x2, &g1)
    la.lindex2(x2, i, -oi, dims, &x3, &g2)
    la.lindex2(x3, j, -oj, dims, &x, &g3)

    u1.mul4(&u[g0], &u[g1], &u[g2], &u[g3], oi, oj, -oi, -oj, r)

cdef void plaq2(long x, long i, long j, long oi, long oj, double *u, long*dims, double*r) nogil:
    # assume oi = 1
    cdef long xs
    if (j-i)*oj > 0:
        xs = x
    else:
        xs = la.shift(x, j, -1, dims)

    plaq_fast(xs, u, dims, r)
    r[1] = oj * r[1]



cdef void plaq_fast(long x, double* u, long* dims, double* r) nogil:
    cdef long xs1, xs2
    xs1 = la.shift(x, 0, 1, dims)
    xs2 = la.shift(x, 1, 1, dims)
    u1.mul4(&u[la.gindex(x, 0)], &u[la.gindex(xs1, 1)], &u[la.gindex(xs2, 0)], &u[la.gindex(x, 1)], 1, 1, -1, -1, r)

cdef void c(long x, long i, long j, long oi, long oj, double* u, long* dims, double* r) nogil:
    cdef long x0, x1, x2, x3, g0, g1, g2, g3
    cdef double b[2]
    x0 = x
    x1 = la.shift(x0, i, oi, dims)
    x2 = x
    x3 = la.shift(x2, j, oj, dims)

    g0 = la.lindex(x0, i, oi, dims)
    g1 = la.lindex(x1, j, oj, dims)
    g2 = la.lindex(x2, j, oj, dims)
    g3 = la.lindex(x3, i, oi, dims)

    u1.mul2(&u[g0], &u[g1], oi, oj, r)
    u1.mul2(&u[g2], &u[g3], oj, oi, &b[0])
    u1.cadd(&b[0], -1, r)