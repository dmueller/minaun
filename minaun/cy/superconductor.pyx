#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

cimport la
cimport lgt
cimport u1
import numpy as np
cimport numpy as cnp
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free
from libc.math cimport sin, cos, exp, sqrt, acos, asin, atan, atan2
cimport openmp
#openmp.omp_set_num_threads(1)

cdef double fourpi = 4.0 * np.pi

class Simulation:
    def __init__(self, nx, ny, q, l, m, mu, h, a):
        self.dims = np.array([nx+2, ny+2], dtype=np.int64)
        self.N = (nx+2) * (ny+2)

        # charge of the scalar field
        self.q = np.double(q)

        # self-interaction coupling
        self.l = np.double(l)

        # mass of the scalar field
        self.m_ph = np.double(m)

        # chemical potential of the scalar field
        self.mu_ph = np.double(mu)

        # external magnetic field
        self.h_ph = np.double(h)

        # dimmless
        self.a = a
        self.make_dimless()

        # gauge links
        self.u  = np.zeros(2*2*(nx+2)*(ny+2), dtype=np.double)
        self.un = np.zeros(2*2*(nx+2)*(ny+2), dtype=np.double)

        # scalar field
        self.p  = np.zeros(2*(nx+2)*(ny+2), dtype=np.double)
        self.pn = np.zeros(2*(nx+2)*(ny+2), dtype=np.double)

        # set all links to unity
        self.u[::2]  = 1.0
        self.un[::2] = 1.0

        # net winding number
        self.w = 0

        # boundary condition
        self.bcs = 'gauge'

        self.compute_parameters()

    def init(self):
        self.apply_bcs()
        self.pn = self.p.copy()
        self.un = self.u.copy()

    def swap(self):
        self.u, self.un = self.un, self.u
        self.p, self.pn = self.pn, self.p

    def set_a(self, a):
        self.a = a
        self.make_dimless()
        self.compute_parameters()

    def make_dimless(self):
        self.m = self.m_ph * self.a
        self.mu = self.mu_ph * self.a
        self.h = self.h_ph * (self.q * self.a ** 2)

    def apply_bcs(self):
        if self.bcs is 'gauge':
            impose_gauge_bcs(self)
        elif self.bcs is 'neumann':
            impose_neumann_bcs(self)
        elif self.bcs is 'periodic':
            impose_periodic_bcs(self)
        else:
            print("Error: unknown boundary condition ", self.bcs)
            exit()

    def compute_parameters(self):
        # DERIVED PARAMETERS AND QUANTITIES

        # gibbs free energy density of pure condensate (lattice units)
        self.g_meissner = 0.25 * (self.mu ** 2 - self.m ** 2) ** 2 / self.l

        # phi^2 of pure condensate (lattice units)
        self.phi_meissner = np.sqrt((self.mu ** 2 - self.m ** 2) / (2.0 * self.l))

        # elementary flux
        self.fluxon = 2 * np.pi / self.q

        # gibbs free energy density of normal phase (lattice units)
        self.g_normal = - (self.a ** 2 *self.h_ph) ** 2 / (8.0 * np.pi)

"""
    Steepest gradient descent method
"""

def gradient_descent(s, double alpha_u, double alpha_p, long iters, double randomizer=0.0):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p, pn, u, un
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        double q = s.q
        double l = s.l
        double m = s.m
        double h = s.h
        double mu = s.mu
        long n = s.N
        long x, i, xf, j, xp, it, ix, iy
        long nx = dims[0]
        long ny = dims[1]
        double *b0
        double *b1
        double *b2
        double *bgf


    for it in range(iters):
        # get current pointers
        p = s.p
        pn = s.pn
        u = s.u
        un = s.un
        s.apply_bcs()

        with nogil, parallel():
            b0 = <double *> malloc(2 * sizeof(double))
            b1 = <double *> malloc(2 * sizeof(double))
            b2 = <double *> malloc(2 * sizeof(double))
            bgf = <double *> malloc(sizeof(double))

            for ix in prange(1, nx-1, schedule='guided'):
                for iy in range(1, ny-1):
                    x = la.index(ix, iy, &dims[0])
                    u1.czero(b1)
                    xf = la.findex(x)

                    # scalar field gradient
                    for i in range(2):
                        lgt.D2(x, i, &p[0], &u[0], &dims[0], b0)
                        u1.cadd(b0, 1.0, b1)
                    u1.cadd(&p[xf], -(m*m - mu*mu), b1)
                    u1.cadd(&p[xf], -2 * l * u1.cabs(&p[xf]), b1)
                    u1.cset(&p[xf], 1, &pn[xf])
                    u1.cadd(b1, alpha_p, &pn[xf])

                    # gauge field gradient
                    for i in range(2):
                        bgf[0] = 0.0
                        j = (i+1) % 2

                        # scalar field contribution
                        xp = la.shift(x, i, 1, &dims[0])
                        lgt.D1b(xp, i, &p[0], &u[0], &dims[0], b2)
                        u1.mul2(b2, &p[la.findex(xp)], -1, +1, b0)
                        bgf[0] = 2.0 * q**2 * b0[1] # take imaginary part

                        # external field contribution
                        lgt.plaq(x, i, j, 1, +1, &u[0], &dims[0], b0)
                        lgt.plaq(x, i, j, 1, -1, &u[0], &dims[0], b1)
                        bgf[0] += h / fourpi * (b0[0] - b1[0]) # take real part

                        # YM contributions
                        lgt.plaq(x, i, j, 1, +1, &u[0], &dims[0], b0)
                        lgt.plaq(x, i, j, 1, -1, &u[0], &dims[0], b1)
                        bgf[0] += - 1.0 / fourpi * (b0[1] + b1[1]) # take imaginary part

                        # evolve
                        u1.cexp(alpha_u * bgf[0], 1, b0)
                        u1.mul2(&u[la.gindex(x, i)], b0, 1, 1, &un[la.gindex(x, i)])

            free(b0)
            free(b1)
            free(b2)
            free(bgf)
        s.swap()
        if randomizer != 0:
            randomize_links(s, randomizer)
            randomize_phi(s, randomizer)
        s.apply_bcs()

"""
    Boundary conditions
"""

def impose_neumann_bcs(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p, pn, u, un
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long xi, yi, nx, ny, i, x1, x2, xs, x
    p = s.p
    u = s.u
    nx = dims[0]
    ny = dims[1]

    # edges of the boundary
    with nogil, parallel():
        # (x,0) boundary
        for xi in prange(1, nx-1):
            x = la.index(xi, 0, &dims[0])
            xs = la.shift(x, 1, +1, &dims[0])
            u1.cset(&p[la.findex(xs)], 1.0, &p[la.findex(x)])

            u1.cunit(&u[la.gindex(x, 1)])
            u1.cset(&u[la.gindex(xs, 0)], 1, &u[la.gindex(x, 0)])

        # (x,ny-1) boundary
        for xi in prange(1, nx-1):
            x = la.index(xi, ny-1, &dims[0])
            xs = la.shift(x, 1, -1, &dims[0])
            u1.cset(&p[la.findex(xs)], 1.0, &p[la.findex(x)])

            u1.cunit(&u[la.gindex(x, 1)])
            u1.cset(&u[la.gindex(xs, 0)], 1, &u[la.gindex(x, 0)])

        # (0,y) boundary
        for yi in prange(1, ny-1):
            x = la.index(0, yi, &dims[0])
            xs = la.shift(x, 0, +1, &dims[0])
            u1.cset(&p[la.findex(xs)], 1.0, &p[la.findex(x)])

            u1.cunit(&u[la.gindex(x, 0)])
            u1.cset(&u[la.gindex(xs, 1)], 1, &u[la.gindex(x, 1)])

        # (nx-1,y) boundary
        for yi in prange(1, ny-1):
            x = la.index(nx-1, yi, &dims[0])
            xs = la.shift(x, 0, -1, &dims[0])
            u1.cset(&p[la.findex(xs)], 1.0, &p[la.findex(x)])

            u1.cunit(&u[la.gindex(x, 0)])
            u1.cset(&u[la.gindex(xs, 1)], 1, &u[la.gindex(x, 1)])

    # corner cells (set to zero)
    for xi in [0, nx-1]:
        for yi in [0, ny-1]:
            x = la.index(xi, yi, &dims[0])
            u1.czero(&p[la.findex(x)])
            for i in range(2):
                u1.cunit(&u[la.gindex(x, i)])

def impose_gauge_bcs(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p, u
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long xi, yi, nx, ny, X, Y, i, k
        double alpha, pi, w, q, px, py, t1, sh
        double* b0
        double* b1
    p = s.p
    u = s.u
    nx = dims[0]
    ny = dims[1]
    pi = np.pi
    w = s.w
    q = 1.0
    with nogil, parallel():
        b0 = <double *> malloc(2 * sizeof(double))
        b1 = <double *> malloc(2 * sizeof(double))

        # PERIODIC BCS
        for xi in prange(nx):
            X = la.index(xi,    0,      &dims[0])
            Y = la.index(xi,    ny-2,   &dims[0])

            for i in range(2):
                u1.cset(&u[la.gindex(Y, i)], 1, &u[la.gindex(X, i)])
            u1.cset(&p[la.findex(Y)], 1, &p[la.findex(X)])

            X = la.index(xi,    ny-1,   &dims[0])

            Y = la.index(xi,    1,      &dims[0])
            for i in range(2):
                u1.cset(&u[la.gindex(Y, i)], 1, &u[la.gindex(X, i)])
            u1.cset(&p[la.findex(Y)], 1, &p[la.findex(X)])

        for yi in prange(ny):
            X = la.index(0,     yi, &dims[0])
            Y = la.index(nx-2,  yi, &dims[0])
            for i in range(2):
                u1.cset(&u[la.gindex(Y, i)], 1, &u[la.gindex(X, i)])
            u1.cset(&p[la.findex(Y)], 1, &p[la.findex(X)])

            X = la.index(nx-1,  yi, &dims[0])
            Y = la.index(1,     yi, &dims[0])
            for i in range(2):
                u1.cset(&u[la.gindex(Y, i)], 1, &u[la.gindex(X, i)])
            u1.cset(&p[la.findex(Y)], 1, &p[la.findex(X)])

        alpha = + w * 2.0 * pi / (ny - 2)

        # SPECIAL GAUGE BCS
        sh = 0.0
        for yi in prange(0, ny):
            X = la.index(0, yi, &dims[0])

            u1.cexp(+alpha * (yi+sh), 1, b0)
            u1.cset(&p[la.findex(X)], 1, b1)
            u1.mul2(b0, b1, 1, 1, &p[la.findex(X)])
            u1.cexp(-alpha, 1, b0)
            u1.cset(&u[la.gindex(X, 1)], 1, b1)
            u1.mul2(b0, b1, 1, 1, &u[la.gindex(X, 1)])

            X = la.index(nx-1, yi, &dims[0])

            u1.cexp(-alpha * (yi+sh), 1, b0)
            u1.cset(&p[la.findex(X)], 1, b1)
            u1.mul2(b0, b1, 1, 1, &p[la.findex(X)])
            u1.cexp(+alpha, 1, b0)
            u1.cset(&u[la.gindex(X, 1)], 1, b1)
            u1.mul2(b0, b1, 1, 1, &u[la.gindex(X, 1)])


        free(b0)
        free(b1)


def impose_periodic_bcs(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p, pn, u, un
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long xi, yi, nx, ny, i, x1, x2, xs, x
        int w
        double alpha
        double q = s.q
        double* b0
        double* b1
    p = s.p
    u = s.u
    nx = dims[0]
    ny = dims[1]
    w = s.w

    alpha = - w * (2.0 * np.pi) / dims[0]

    # edges of the boundary
    with nogil, parallel():
        b0 = <double *> malloc(2 * sizeof(double))
        b1 = <double *> malloc(2 * sizeof(double))

        # (x,0) boundary
        for xi in prange(nx):
            x = la.index(xi,  0,    &dims[0])
            xs = la.index(xi, ny-2, &dims[0])

            u1.cset(&p[la.findex(xs)], 1, &p[la.findex(x)])
            u1.cset(&u[la.gindex(xs, 0)], 1, &u[la.gindex(x, 0)])
            u1.cset(&u[la.gindex(xs, 1)], 1, &u[la.gindex(x, 1)])

        # (x,ny-1) boundary
        for xi in prange(nx):
            x = la.index(xi,  ny-1, &dims[0])
            xs = la.index(xi, 1,    &dims[0])

            u1.cset(&p[la.findex(xs)], 1, &p[la.findex(x)])
            u1.cset(&u[la.gindex(xs, 0)], 1, &u[la.gindex(x, 0)])
            u1.cset(&u[la.gindex(xs, 1)], 1, &u[la.gindex(x, 1)])

        # (0,y) boundary
        for yi in prange(ny):
            x = la.index(0,     yi, &dims[0])
            xs = la.index(nx-2, yi, &dims[0])

            u1.cset(&p[la.findex(xs)], 1, &p[la.findex(x)])
            u1.cset(&u[la.gindex(xs, 0)], 1, &u[la.gindex(x, 0)])
            u1.cset(&u[la.gindex(xs, 1)], 1, &u[la.gindex(x, 1)])

        # (nx-1,y) boundary
        for yi in prange(ny):
            x = la.index(nx-1, yi, &dims[0])
            xs = la.index(1,   yi, &dims[0])

            u1.cset(&p[la.findex(xs)], 1, &p[la.findex(x)])
            u1.cset(&u[la.gindex(xs, 0)], 1, &u[la.gindex(x, 0)])
            u1.cset(&u[la.gindex(xs, 1)], 1, &u[la.gindex(x, 1)])

        free(b0)
        free(b1)


"""
    Randomizer functions
"""

def randomize_links(s, double amplitude = 1.0):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] u = s.u
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        cnp.ndarray[double, ndim=1, mode="c"] phases = amplitude * (2.0 * np.random.random(2*s.N) - 1.0) * fourpi
        cnp.ndarray[double, ndim=1, mode="c"] cos_f = np.cos(phases)
        cnp.ndarray[double, ndim=1, mode="c"] sin_f = np.sin(phases)
        long n = s.N
        long x, i
        double *b0
        double *b1

    for x in prange(n, nogil=True):
        b0 = <double *> malloc(2 * sizeof(double))
        b1 = <double *> malloc(2 * sizeof(double))
        for i in range(2):
            b0[0] = cos(phases[2*x+i])
            b0[1] = sin(phases[2*x+i])
            u1.mul2(&u[la.gindex(x, i)], b0, 1, 1, b1)
            u1.cset(b1, 1, &u[la.gindex(x, i)])
        free(b0)
        free(b1)

    s.init()


def randomize_phi(s, double amplitude = 1.0):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p = s.p
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        cnp.ndarray[double, ndim=1, mode="c"] phases = amplitude * (2.0 * np.random.random(s.N) - 1.0) * fourpi
        cnp.ndarray[double, ndim=1, mode="c"] cos_f = np.cos(phases)
        cnp.ndarray[double, ndim=1, mode="c"] sin_f = np.sin(phases)
        long n = s.N
        long x
        double *b0
        double *b1

    for x in prange(n, nogil=True):
        b0 = <double *> malloc(2 * sizeof(double))
        b1 = <double *> malloc(2 * sizeof(double))
        b0[0] = cos_f[x]
        b0[1] = sin_f[x]
        u1.mul2(&p[la.findex(x)], b0, 1, 1, b1)
        u1.cset(b1, 1, &p[la.findex(x)])
        free(b0)
        free(b1)

    s.init()
