#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    A collection of initial conditions
"""

cimport la
cimport lgt
cimport u1
import numpy as np
cimport numpy as cnp
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free
from libc.math cimport sin, cos, exp, sqrt, acos, asin, atan, atan2, round

"""
    Set scalar field to a homogenous type I condensate
"""
def type1_condensate(s, double phase = 0.0):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] p = s.p
        double condensate = np.sqrt((s.mu ** 2 - s.m ** 2) / (2.0 * s.l))
        long n = s.N
        long x

    for x in prange(n, nogil=True):
        p[la.findex(x)+0] = condensate * cos(phase)
        p[la.findex(x)+1] = condensate * sin(phase)

    s.init()

"""
    Add a vortex solution to the scalar field
"""
def make_vortex(s, long x0, long y0, double radius, int winding=1):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] u = s.u
        cnp.ndarray[double, ndim=1, mode="c"] p = s.p
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long n = s.N
        long nx = dims[0]
        long ny = dims[1]
        long x, y, xi, i
        double *b0
        double *b1
        double value, dx, dy, d2, phase

    for x in prange(nx, nogil=True):
        b0 = <double *> malloc(2 * sizeof(double))
        b1 = <double *> malloc(2 * sizeof(double))
        for y in range(ny):
            xi = la.index(x, y, &dims[0])
            dx = (<double> (x + 0.5 - x0))
            dy = (<double> (y + 0.5 - y0))
            d2 = (dx ** 2 + dy ** 2)

            # scalar field hole
            value = 1.0 - exp(-d2/radius**2)
            phase = +winding*atan2(dy, dx)

            p[la.findex(xi)+0] *= value
            p[la.findex(xi)+1] *= value

            u1.cexp(phase, 1, b0)
            u1.mul2(b0, &p[la.findex(xi)], 1, 1, b1)
            u1.cset(b1, 1, &p[la.findex(xi)])

        free(b0)
        free(b1)

    s.init()

"""
    Set gauge field to a flux tube solution
"""
def make_flux_tube(s, long x0, long y0, double radius, double depth, int winding=1):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] u = s.u
        cnp.ndarray[double, ndim=1, mode="c"] p = s.p
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long n = s.N
        long nx = dims[0]
        long ny = dims[1]
        long x, y, xi, i
        double *b0
        double *b1
        double value, dx, dy, d2, phase, g

    for x in prange(nx, nogil=True):
        b0 = <double *> malloc(2 * sizeof(double))
        b1 = <double *> malloc(2 * sizeof(double))
        for y in range(ny):
            xi = la.index(x, y, &dims[0])
            dx = (<double> (x + 0.5 - x0))
            dy = (<double> (y + 0.5 - y0))
            d2 = (dx ** 2 + dy ** 2)

            # scalar field hole
            value = 1.0 - exp(-d2/radius**2)
            phase = +winding*atan2(dy, dx)

            p[la.findex(xi)+0] *= value
            p[la.findex(xi)+1] *= value

            u1.cexp(phase, 1, b0)
            u1.mul2(b0, &p[la.findex(xi)], 1, 1, b1)
            u1.cset(b1, 1, &p[la.findex(xi)])

            # set gauge field
            g = - winding*depth / (d2 / radius ** 2) * (1.0 - exp(-0.5 * (d2 / radius ** 2)))

            u1.cset(&u[la.gindex(xi, 0)], 1, b0)
            u1.cexp(- dy * g, 1, b1)
            u1.mul2(b0, b1, 1, 1, &u[la.gindex(xi, 0)])

            u1.cset(&u[la.gindex(xi, 1)], 1, b0)
            u1.cexp(+ dx * g, 1, b1)
            u1.mul2(b0, b1, 1, 1, &u[la.gindex(xi, 1)])



        free(b0)
        free(b1)

    s.init()

"""
    Constant magnetic field guess for gauge boundary conditions
"""

def initial_gauge(s, double phi):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] u = s.u
        cnp.ndarray[double, ndim=1, mode="c"] p = s.p
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long n = s.N
        long nx = dims[0]
        long ny = dims[1]
        long x, y, xi, i, yi, x0, y0
        double *b0
        double *b1
        double value, dx, dy, d2, phase, alpha, w, pi
    pi = np.pi
    w = s.w
    alpha = - 2.0 * pi * w / (nx-2)

    for xi in prange(nx, nogil=True):
        b0 = <double *> malloc(2 * sizeof(double))
        b1 = <double *> malloc(2 * sizeof(double))
        for yi in range(ny):
            x = la.index(xi, yi, &dims[0])

            # 0.25, 0.75, DOWNWARD
            x0 = <int> round(0.25 * (nx-2)) + 1
            y0 = <int> round(0.75 * (ny-2)) + 1
            dx = (<double> (xi - x0))
            dy = (<double> (yi - y0))
            if dx < 0:
                phase = -2*atan2(dy, dx)
            else:
                phase = pi

            u1.cexp(phase, phi, b0)
            u1.mul2(b0, &p[la.findex(x)], 1, 1, b1)
            u1.cset(b1, 1, &p[la.findex(x)])

            # 0.75, 0.25, UPWARD
            x0 = <int> round(0.75 * (nx-2)) + 1
            y0 = <int> round(0.25 * (ny-2)) + 1
            dx = (<double> (xi - x0))
            dy = (<double> (yi - y0))
            if dx < 0:
                phase = -2*atan2(dy, dx)
            else:
                phase = pi

            u1.cexp(phase, phi, b0)
            u1.mul2(b0, &p[la.findex(x)], 1, 1, b1)
            u1.cset(b1, 1, &p[la.findex(x)])

        free(b0)
        free(b1)
    s.w = +2.0
    s.init()