"""
    A script for simulation a hexagonal lattice of flux tubes using
    the periodic Ginzburg-Landau (Abelian Higgs) model.

    Special 'gauge' boundary conditions are used to simulate the
    quasi-periodic property of gauge field and complex scalar field.
"""

import minaun.cy.superconductor as superconductor
import minaun.cy.observables as observables
import minaun.cy.initial as initial
import numpy as np
import matplotlib.pyplot as plt
import minaun.utils.plots
from time import time

# LATTICE SIZE
nx = 64
al = np.sqrt(3)
ny = int(round(nx*al))

# PHYSICAL PARAMETERS
a = 30.0 / nx
m = 0
q = 0.2
l = 0.5
mu = 1.0
h = 3.5

# GRADIENT DESCENT OPTIONS
alpha_p = 0.2
alpha_u = alpha_p
iterations_per_step = 1000
total_steps = 20000

# AREA CHANGE OPTIONS
change_min_steps = 4
change_max_steps = 40
change_factor = 0.9

# OUTPUT OPTIONS
filename_b = "/home/dmueller/data/run_0_B.dat"
filename_p = "/home/dmueller/data/run_0_P.dat"

# DISPLAY OPTIONS
tile_x = 3
tile_y = 2

# INIT
s = superconductor.Simulation(nx=nx, ny=ny, q=q, l=l, m=m, mu=mu, h=h, a=a)
s.bcs = 'gauge'

# INITIAL CONDITION FOR HEXAGONAL LATTICE
initial.type1_condensate(s)
initial.initial_gauge(s, s.phi_meissner)

# SIMULATION LOOP
g0 = observables.free_energy(s) / s.g_meissner
t0 = t1 = 0

g_list = []
timer = 0

plt.ion()
for i in range(total_steps):
    # DATA
    P = observables.phi_abs(s)
    B = observables.magnetic_field(s)
    p_complex = minaun.utils.plots.phi_phase_plot(s)
    g1 = observables.free_energy(s) / s.g_meissner

    # OUTPUT
    B.astype('float64').tofile(filename_b)
    P.astype('float64').tofile(filename_p)

    # DISPLAY
    plt.cla()
    plt.imshow(np.tile(B, [tile_x, tile_y]), cmap=plt.get_cmap('seismic'))
    plt.imshow(p_complex, cmap=plt.get_cmap('jet'))
    levels = np.arange(-0.1, 1.1, 0.05)
    plt.contour(np.tile(np.log(P/s.phi_meissner+1), [tile_x, tile_y]))
    plt.pause(0.001)

    # TERMINAL OUTPUT
    sg = np.sign(g1-g0)
    print("G", g1, "G_n", s.g_normal/s.g_meissner, "w:", np.sum(B) / s.fluxon, "t [s]", t1-t0, "inc/dec", np.sign(g1-g0))
    g0 = g1

    # GRADIENT DESCENT
    t0 = time()
    superconductor.gradient_descent(s, alpha_u, alpha_p, iterations_per_step, 0.0)
    t1 = time()

    # CHANGE LATTICE SPACING
    timer += 1
    if (sg > 0.0 and timer > change_min_steps) or timer > change_max_steps:
        timer = 0
        g_list.append([observables.free_energy(s) / s.g_meissner, s.a*nx])
        s.set_a(s.a * change_factor)
        print("Decrease size! L = ", s.a * nx)
        print("Results:")
        print("**********************")
        for g in g_list:
            print("{0}\t {1}".format(g[1], g[0]))
        print("**********************")
