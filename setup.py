from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import Extension
import numpy

ext = Extension(name="*",
                sources=["minaun/cy/*.pyx"],
                extra_compile_args=["-Ofast", "-O3", "-fopenmp", "-march=native", "-w"],
                extra_link_args=['-fopenmp'],
                include_dirs=[numpy.get_include()],
                )

extensions = [ext]

setup(
    ext_modules=cythonize(extensions), requires=['numpy', 'matplotlib', 'Cython']
)
